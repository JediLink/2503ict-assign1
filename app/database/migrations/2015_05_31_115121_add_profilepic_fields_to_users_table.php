<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddProfilepicFieldsToUsersTable extends Migration {

    /**
     * Make changes to the table.
     *
     * @return void
     */
    public function up()
    {   
        Schema::table('users', function(Blueprint $table) {     
            
            $table->string('profilepic_file_name')->nullable();
            $table->integer('profilepic_file_size')->nullable();
            $table->string('profilepic_content_type')->nullable();
            $table->timestamp('profilepic_updated_at')->nullable();

        });

    }

    /**
     * Revert the changes to the table.
     *
     * @return void
     */
    public function down()
    {
        
    }

}
