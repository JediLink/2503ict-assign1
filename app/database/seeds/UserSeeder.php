<?php

class UserSeeder extends Seeder
{
    public function run()
    {
        $user = new User;
        $user->email = '1@gmail.com';
        $user->password = Hash::make('password');
        $user->fullname = 'Cowington McMoo';
        $user->dob = '2000-01-01';
        $user->save();
        
        $user = new User;
        $user->email = '2@gmail.com';
        $user->password = Hash::make('password');
        $user->fullname = 'Shaun the Sheep';
        $user->dob = '2005-12-09';
        $user->save();
        
        $user = new User;
        $user->email = '3@gmail.com';
        $user->password = Hash::make('password');
        $user->fullname = 'Sir Porkins';
        $user->dob = '1960-01-08';
        $user->save();
        
        $user = new User;
        $user->email = '4@gmail.com';
        $user->password = Hash::make('password');
        $user->fullname = 'Horace';
        $user->dob = '2002-04-22';
        $user->save();
        
        $user = new User;
        $user->email = '5@gmail.com';
        $user->password = Hash::make('password');
        $user->fullname = 'Mike Bison';
        $user->dob = '1966-06-30';
        $user->save();
        
        $user = new User;
        $user->email = '6@gmail.com';
        $user->password = Hash::make('password');
        $user->fullname = 'Henrietta';
        $user->dob = '1998-02-23';
        $user->save();
        
        $user = new User;
        $user->email = '7@gmail.com';
        $user->password = Hash::make('password');
        $user->fullname = 'Doge';
        $user->dob = '2013-11-13';
        $user->save();
        
        $user = new User;
        $user->email = '8@gmail.com';
        $user->password = Hash::make('password');
        $user->fullname = 'Mama Llama';
        $user->dob = '1237-05-02';
        $user->save();
        
        $user = new User;
        $user->email = '9@gmail.com';
        $user->password = Hash::make('password');
        $user->fullname = 'Duck Quackers';
        $user->dob = '1997-02-23';
        $user->save();
        
        $user = new User;
        $user->email = '10@gmail.com';
        $user->password = Hash::make('password');
        $user->fullname = 'The GOAT';
        $user->dob = '1991-10-10';
        $user->save();
    }
}