<?php

class PostSeeder extends Seeder
{
    public function run()
    {
        $post = new Post;
        $post->title = 'This is a friends-only post.';
        $post->message = 'If you can see this, you must be a friend.';
        $post->visibility = 1;
        $post->user_id = 1;
        $post->save();
        
        $post = new Post;
        $post->title = 'Do androids dream of electric cows?';
        $post->message = "I'd sincerely hope so. Cows are the best. Much better than sheep.";
        $post->visibility = 2;
        $post->user_id = 1;
        $post->save();
        
        for ($i = 0; $i < 8; $i++)
        {
            $post = new Post;
            $post->title = 'This is a private post.';
            $post->message = 'Only I can see this when I am logged in.';
            $post->visibility = 0;
            $post->user_id = 1;
            $post->save();
        }
    }
}