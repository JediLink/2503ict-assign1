--Drop the tables if they exist
drop table if exists posts;
drop table if exists comments;

--Create the tables
create table posts (
    id integer not null primary key autoincrement,
    author varchar(255) not null,
    title varchar(255) not null,
    message text not null
);
create table comments (
    id integer not null primary key autoincrement,
    author varchar(255) not null,
    message text not null,
    postid integer not null
);

--Insert some initial data
insert into posts values (null, "Cowington McMoo", "Grass", "Have you guys ever tried the grass on the other side of the meadow? I've heard good things about it.");

--Test query
select * from posts;