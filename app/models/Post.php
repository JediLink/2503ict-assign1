<?php

class Post extends Eloquent {
    public function user() {return $this->belongsTo('User');}
    
    public static $rules = array(
        'title' => 'required',
        'message' => 'required'
    );
}