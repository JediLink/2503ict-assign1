<?php

class Comment extends Eloquent {
    public function user() {return $this->belongsTo('User');}
    
    public static $rules = array(
        'message' => 'required'
    );
}