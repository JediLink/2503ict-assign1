<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class User extends Eloquent implements UserInterface, RemindableInterface, StaplerableInterface {

	use UserTrait, RemindableTrait, EloquentTrait;
	
	public function __construct(array $attributes = array()) {
		$this->hasAttachedFile('profilepic', ['styles' => ['thumb' => '100x100', 'icon' => '16x16']]);
		parent::__construct($attributes);
	}

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	
	public static $loginrules = array(
        'email' => 'required',
        'password' => 'required'
    );
    public static $signuprules = array(
        'email' => 'required|email|unique:users',
        'fullname' => 'required',
        'password' => 'required',
        'dob' => 'required',
        'profilepic' => 'image'
    );
	
	//Relation definitions
	public function posts() {return $this->hasMany('Post');}
	public function comments() {return $this->hasMany('Comment');}
	public function friends() {return $this->belongsToMany('User', 'friendships', 'user1_id', 'user2_id');}
}
