<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function friendslist($id)
	{
		$person = User::find($id);
		$users = DB::select('
			SELECT users.*
			FROM users, friendships
			WHERE ((friendships.user1_id = ? AND friendships.user2_id = users.id) OR (friendships.user2_id = ? AND friendships.user1_id = users.id))
		', array($id, $id));
		
		return View::make('layouts.userlist', compact('person'), compact('users'));
	}
	
	public function search()
	{
		$input = Input::all();
		$keyword = $input['keyword'];
		$users = DB::table('users')->where('fullname', 'LIKE', "%$keyword%")->get();
		return View::make('layouts.userlist', compact('users'));
	}
	 
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('layouts.signup');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$v = Validator::make($input, User::$signuprules);
		if ($v->passes())
		{
			$password = $input['password'];
			$encrypted = Hash::make($password);
			$user = new User;
			$user->email = $input['email'];
			$user->password = $encrypted;
			$user->dob = $input['dob'];
			$user->fullname = $input['fullname'];
			$user->profilepic = $input['profilepic'];
			$user->save();
			
			if (Auth::attempt(array('email' => $input['email'], 'password' => $input['password'])))
			{
				return Redirect::to(secure_url('post'));
			}
		}
		else
		{
			// Show validation errors
			return Redirect::action('UserController@create')->withErrors($v);
		}
	}
	
	public function login()
	{
		$input = Input::all();
		$v = Validator::make($input, User::$loginrules);
		if ($v->passes())
		{
			$email = $input['email'];
			$password = $input['password'];
			
			if (Auth::attempt(array('email' => $email, 'password' => $password)))
			{
				return Redirect::to(secure_url('post'));
			}
			else
			{
				Session::put('login_error', 'Login failed');
				return Redirect::to(URL::previous());
			}
		}
		else
		{
			// Show validation errors
			return Redirect::to(URL::previous())->withErrors($v);
		}
	}
	
	public function logout()
	{
		Session::flush();
		Auth::logout();
		return Redirect::to(url('post'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::find($id);
		$posts = DB::table('posts')->where('user_id', $id)->get();
		return View::make('layouts.viewprofile', compact('user'), compact('posts'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);
		return View::make('layouts.editprofile', compact('user'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
		$user = User::find($id);
		$v = Validator::make($input, array('email' => 'required|email|unique:users,email,'.$id, 'fullname' => 'required', 'dob' => 'required', 'profilepic' => 'image'));
		if ($v->passes())
		{
			$user->dob = $input['dob'];
			$user->fullname = $input['fullname'];
			$user->email = $input['email'];
			$password = $input['password'];
			if ($password != '') {
				$encrypted = Hash::make($password);
				$user->password = $encrypted;
			}
			if ($input['profilepic'] != null) {	$user->profilepic = $input['profilepic']; }
			$user->save();
			return Redirect::route('user.show', $user->id);
		}
		else
		{
			// Show validation errors
			return Redirect::action('UserController@edit', array($user->id))->withErrors($v);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
