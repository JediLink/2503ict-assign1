<?php

class FriendshipController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$friendship = new Friendship();
		$friendship->user1_id = $input['user1_id'];
		$friendship->user2_id = $input['user2_id'];
		$friendship->save();
		return Redirect::route('user.show', array($input['user2_id']));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$userid;
		if (Friendship::find($id)->user1_id == Auth::check())
			{ $userid = Friendship::find($id)->user2_id; }
		else
			{ $userid = Friendship::find($id)->user1_id; }
		
		Friendship::destroy($id);
		return Redirect::route('user.show', array($userid));
	}


}
