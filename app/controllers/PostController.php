<?php

class PostController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$userid = null;
		if (Auth::check()) {$userid = Auth::user()->id;}
		
		//One single query
		//Retrieves if it's your own post, it's public, or it's friends only if you're friends with the poster
		$posts = DB::SELECT('
			SELECT DISTINCT posts.*
			FROM posts
			LEFT JOIN friendships
			WHERE
				posts.user_id = ?
				OR visibility = 2
				OR (visibility = 1 AND ((friendships.user1_id = ? AND friendships.user2_id = posts.user_id) OR (friendships.user2_id = ? AND friendships.user1_id = posts.user_id)))
			ORDER BY posts.id DESC
		', array($userid, $userid, $userid));
		
		//This is commented out because it doesn't pass the ID for some reason because Laravel sucks even though it's the exact same query
		/*$posts = DB::table('posts')
		->leftJoin('friendships', 'posts.user_id', '=', 'friendships.user1_id')
		->whereRaw('
			posts.user_id = ?
			OR visibility = 2
			OR (visibility = 1 AND ((friendships.user1_id = ? AND friendships.user2_id = posts.user_id) OR (friendships.user2_id = ? AND friendships.user1_id = posts.user_id)))
			', array($userid, $userid, $userid))
		->orderBy('posts.id', 'desc')
		->get();*/
		
 		return View::make('layouts.home', compact('posts'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$post = new Post();
		$input = Input::all();
		$v = Validator::make($input, Post::$rules);
		if ($v->passes())
		{
			$post->title = $input['title'];
			$post->message = $input['message'];
			$post->visibility = $input['visibility'];
			$post->user_id = Auth::user()->id;
			$post->save();
			return Redirect::to(secure_url('post'));
		}
		else
		{
			// Show validation errors
			return Redirect::secure_url('post', array($post->id))->withErrors($v);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$post = Post::find($id);
		$comments = DB::table('comments')->where('post_id', $id)->limit(8)->paginate(8);
		return View::make('layouts.comments', compact('post'), compact('comments'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$post = Post::find($id);
		return View::make('layouts.edit', compact('post'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$post = Post::find($id);
		$input = Input::all();
		$v = Validator::make($input, Post::$rules);
		if ($v->passes())
		{
			$post->title = $input['title'];
			$post->message = $input['message'];
			$post->visibility = $input['visibility'];
			$post->save();
			return Redirect::route('post.show', $post->id);
		}
		else
		{
			// Show validation errors
			return Redirect::action('PostController@edit', array($post->id))->withErrors($v);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Comment::whereRaw('post_id = ?', array($id))->delete(); //Destroy all comments on the post
		Post::destroy($id); //Destroy the post
		return Redirect::to('post');
	}
	
}
