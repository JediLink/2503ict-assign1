<?php

/*
|--------------------------------------------------------------------------
| Application Routes (ASSIGNMENT 2)
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//////////////////////////////////////////////
// Page load (home/comments/edit/docs)
//////////////////////////////////////////////

//Redirect to home
Route::get('/', function()
{
	return Redirect::to(secure_url('post'));
});

//Resources
Route::post('user/login', array('as' => 'user.login', 'uses' => 'UserController@login'));
Route::get('user/logout', array('as' => 'user.logout', 'uses' => 'UserController@logout'));
Route::get('user/friendslist/{id}', array('as' => 'user.friendslist', 'uses' => 'UserController@friendslist'));
Route::post('user/search', array('as' => 'user.search', 'uses' => 'UserController@search'));
Route::resource('post', 'PostController');
Route::resource('comment', 'CommentController');
Route::resource('user', 'UserController');
Route::resource('friendship', 'FriendshipController');

//Link to the documents page
Route::get('documents', function()
{
  return View::make('layouts.documents');
});

//Link to the login page
Route::get('login', function()
{
  return View::make('layouts.login');
});